@extends('layout')
@section('title', 'Mijn projecten')

@section('content')
    <div class="container">

        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <div class="alert margin-top-high" id="taskMessageDiv" style="display: none">
            <p id="taskMessage"></p>
        </div>

        <h1 class="padding-top-normal">{{$project->title}}</h1>

        <p class="padding-top-normal">{{$project->description}}</p>


        <div class="d-flex bd-highlight">
            <h1 class="w-100 bd-highlight">Tasks:</h1>
            <button type="submit" data-toggle="modal" onclick="reply_click(this.id)" data-target="#Taskmodal" class="button-small-yellow p-2 flex-shrink-1 bd-highlight"><span>New task </span></button>
        </div>

        <p id="taskItems"></p>
    </div>

    <p id="project_id" hidden>{{$project->id}}</p>



    <div class="modal fade" id="Taskmodal" tabindex="-1" role="dialog" aria-labelledby="TaskmodalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="TaskmodalLabel">Add a new task</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form method="post" id="FormAddTask">
                        @csrf
                        <input type="hidden" id="InputId" name="id">
                        <div class="form-group">
                            <label for="InputTask">Task:</label>
                            <input type="text" name="description" class="form-control" id="InputTask" placeholder="Your task">
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="button-small-blue" onclick="add(document.querySelector('#InputTask').value)"><span>Add</span></button>
                    <button type="submit" class="button-small-red" data-dismiss="modal"><span>Close</span></button>
                </div>
            </div>
        </div>
    </div>
    <?php $user = auth()->user() ?>
    <p id="project_id_user" hidden><?= $user->id ?></p>

    <script type="text/javascript">
        var id = document.querySelector('#project_id').innerHTML;
        getData();
        checkData();

        function checkData() {
            $.ajax({
                method: "get",
                url: "/check/" + id,
                success: function (response) {
                    let result = JSON.parse(response);

                    let user_id = document.querySelector('#project_id_user').innerHTML;

                    if(result.project[0].owner_id != user_id) {
                        window.location.href = "/";
                    }
                },
                error: function (error) {
                    console.log("Error: " + error);
                }
            });
        }

        function getData() {
            $.ajax({
                type: "GET",
                url: "/get-data/" + id,
                success: function (response) {
                    let result = JSON.parse(response);
                    let Data = "";
                    for (let i = 0; i < result.task.length; i++) {
                        Data += ("<div class=\"col-lg-12\">");
                        Data += ("<div class=\"custom-control custom-checkbox\">");

                        Data += ("<input type=\"checkbox\" onchange=\"update(this.id, this.checked, this.name)\" " + result.task[i].completed + " name=" + result.task[i].description + " class=\"custom-control-input\" id=" + result.task[i].id + ">");

                        Data += ("<label class=\"custom-control-label noselect\" for=" + result.task[i].id + ">" + result.task[i].description + "</label>");
                        Data += ("</div>");
                        Data += ("</div>");
                    }
                    document.querySelector('#taskItems').innerHTML = Data;
                },
                error: function (error) {
                    console.log("Error: " + error);
                }
            });
        }

        function reply_click(clicked_id) {
            document.querySelector('#InputId').value = clicked_id;
            document.querySelector('#FormAddTask').action = "/task/" + clicked_id + "/add";
            document.querySelector('#InputTask').value = '';

        }

        function add(description) {
            let id = document.querySelector('#project_id').innerHTML;
            $.ajax({
                method: "post",
                url: "/add/" + id + "/" + description,
                data: {_token: "{{csrf_token()}}"},
                success: function () {
                    $('#Taskmodal').modal('hide');
                    toastr.success("Task: " + description +", has been succesfully added");
                    getData();
                },
                error: function (error) {
                    console.log("Error: " + error);
                }
            });
        }

        function update(id, checked, description) {
            $.ajax({
                method: "post",
                url: "/update/" + id + "/" + checked,
                data: {_token: "{{csrf_token()}}"},
                success: function (response) {
                    if (response === "true") {
                        toastr.success(description + " has been successfully marked");
                    } else {
                        toastr.success(description + " has been successfully unmarked");
                    }
                    getData();
                },
                error: function () {
                    toastr.error("Something went wrong while updating task: "+ description);
                }
            });
        }


        function formValidateTitle(id) {
            let title = document.querySelector("#"+id);

            if (title.value.length <= 0) {
                title.classList.remove('is-valid');
                title.classList.add('is-invalid');
            } else {
                title.classList.remove('is-invalid');
                title.classList.add('is-valid');
            }
        }

        function formValidateDescription(id) {
            let description = document.querySelector("#"+id);

            if (description.value.length <= 0) {
                description.classList.remove('is-valid');
                description.classList.add('is-invalid');
            } else {
                description.classList.remove('is-invalid');
                description.classList.add('is-valid');
            }
        }

        function formValidate(titleId, descriptionId, action) {
            let title = document.querySelector("#"+titleId);
            let description = document.querySelector("#"+descriptionId);

            if (title.value.length > 0 && description.value.length > 0) {
                if(action === "add") {
                    addProject(title.value, description.value);
                } else {
                    let id = document.querySelector('#projectChangeId').value;
                    let title = document.querySelector('#inputTitleProjectChange').value;
                    let description = document.querySelector('#inputdescriptionProjectChange').value;
                    changeProject(id, title, description);
                }
            } else {
                if (title.value.length <= 0) {
                    title.classList.add('is-invalid');
                }
                if (description.value.length <= 0) {
                    description.classList.add('is-invalid');
                }
            }
        }
    </script>
@endsection

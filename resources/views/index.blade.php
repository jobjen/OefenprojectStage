@extends('layout')
@section('title', 'My project')

@section('content')
    <div class="container-fluid">

        <div class="d-flex bd-highlight padding-top-normal">
            <h1 class="w-100 bd-highlight">My projects</h1>

            <button type="submit" data-toggle="modal" data-target="#projectAddModal" class="button-small-yellow p-2 flex-shrink-1 bd-highlight"><span>Add a project</span></button>
        </div>

        <div id="ProjectItems"></div>

    </div>

    <div class="modal fade" id="projectAddModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add a new project</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label for="inputTitle">Project title</label>
                        <input type="text" name="title" onkeyup="formValidateTitle(this.id)" id="inputTitle" class="form-control" placeholder="Project title">
                        <div class="invalid-feedback"><i class="fas fa-times"></i>&nbsp; Fill in a valid title</div>
                        <div class="valid-feedback"><i class="fas fa-check"></i></div>
                    </div>

                    <div class="form-group">
                        <label for="inputdescription">Project description</label>
                        <textarea name="description" onkeyup="formValidateDescription(this.id)" id="inputDescription"
                                  class="form-control" placeholder="Project description"></textarea>
                        <div class="invalid-feedback"><i class="fas fa-times"></i>&nbsp; Fill in a valid description
                        </div>
                        <div class="valid-feedback"><i class="fas fa-check"></i></div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="button-small-blue" onclick="formValidate('inputTitle', 'inputDescription', 'add')"><span>Add</span></button>
                    <button type="submit" class="button-small-red" data-dismiss="modal"><span>Close</span></button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="projectChangeModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Change: </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <input type="hidden" id="projectChangeId">
                    <div class="form-group">
                        <label for="inputTitle">The title of the project</label>
                        <input type="text" class="form-control" name="title" id="inputTitleProjectChange" onkeyup="formValidateTitle(this.id)" required>
                        <div class="invalid-feedback"><i class="fas fa-times"></i>&nbsp; Fill in a valid title</div>
                        <div class="valid-feedback"><i class="fas fa-check"></i></div>
                    </div>

                    <div class="form-group">
                        <label for="inputdescription">The description of the project</label>
                        <textarea type="text" class="form-control" name="description" id="inputdescriptionProjectChange" onkeyup="formValidateDescription(this.id)" rows="5"></textarea>
                        <div class="invalid-feedback"><i class="fas fa-times"></i>&nbsp; Fill in a valid description
                        </div>
                        <div class="valid-feedback"><i class="fas fa-check"></i></div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="button-small-blue" onclick="formValidate('inputTitleProjectChange', 'inputdescriptionProjectChange', 'change')"><span>Change</span></button>
                    <button type="submit" class="button-small-red" data-dismiss="modal"><span>Close</span></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Change: </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <form method="POST" action="{{ route('login') }}" id="loginForm">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" id="loginSubmitButton" onclick="document.querySelector('#loginForm').submit()" class="button-small-blue">
                        <span>{{ __('Login') }}</span>
                    </button>

                    @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <script>
        @if(!Auth::check())
            $('#loginModal').modal('show');

            var input = document.getElementById("password");
            input.addEventListener("keyup", function(event) {
                if (event.keyCode === 13) {
                    event.preventDefault();
                    document.getElementById("loginSubmitButton").click();
                }
            });
        @else()
            getData();
        @endif

        function getData() {
            $.ajax({
                method: "GET",
                url: "/getProjectData/",
                success: function (response) {
                    let result = JSON.parse(response);
                    let Data = "";

                    Data += ("<table class=\"table table-hover table-striped margin-top-high\">");
                    Data += ("<thead>");
                    Data += ("<tr>");
                    Data += ("<th scope=\"col\" style=\"width: 25%\">Project name</th>");
                    Data += ("<th scope=\"col\" style=\"width: 60%\">Project description</th>");
                    Data += ("<th scope=\"col\" style=\"width: 5%\"></th>");
                    Data += ("<th scope=\"col\" style=\"width: 5%\"></th>");
                    Data += ("<th scope=\"col\" style=\"width: 5%\"></th>");
                    Data += ("</tr>");
                    Data += ("</thead>");
                    Data += ("<tbody>");

                    for (let i = 0; i < result.project.length; i++) {
                        Data += ("<tr>");

                        Data += ("<td>" + result.project[i].title + "</td>");
                        Data += ("<td>" + result.project[i].description + "</td>");

                        Data += ("<td class='text-right'>");
                        Data += ("<a href=\"/project/" + result.project[i].id + "\" class=\"color\"><button type=\"submit\" class=\"button-small-green\"><span>View</span></button></a>");
                        Data += ("</td>");

                        Data += ("<td class='text-right'>");

                        Data += ("<button type=\"submit\" onclick='getModalData(this.id)' id="+ result.project[i].id +" class=\"button-small-blue\"><span>Edit</span></button>");

                        Data += ("</td>");

                        Data += ("<td class='text-right'>");

                        Data += ("<a onclick=\"deleteProjct(this.id)\" id=" + result.project[i].id + " class=\"color\"><button type=\"button\" class=\"button-small-red\"><span>Delete</span></button></a>");

                        Data += ("</td>");
                        Data += ("</tr>");
                    }

                    Data += ("</tbody>");
                    Data += ("</table>");
                    document.querySelector('#ProjectItems').innerHTML = Data;
                },
                error: function (error) {
                    console.log("Error: " + error);
                }
            });
        }

        function getModalData(id) {
            let projectChangeId = document.querySelector('#projectChangeId');
            projectChangeId.value = id;

            let inputTitleProjectChange = document.querySelector('#inputTitleProjectChange');
            let inputdescriptionProjectChange = document.querySelector('#inputdescriptionProjectChange');

            $.ajax({
                method: "get",
                url: "/getProjectDataById/" + id,
                success: function (response) {
                    let result = JSON.parse(response);
                    let title = result.project[0].title;
                    let description = result.project[0].description;

                    inputTitleProjectChange.value = title;
                    inputdescriptionProjectChange.value = description;
                    $('#projectChangeModal').modal('show');
                },
                error: function (error) {
                    console.log("Error: " + error);
                }
            });
        }

        function addProject(title, description) {
            $.ajax({
                method: "post",
                url: "/project/add/" + title + "/" + description,
                data: {_token: "{{csrf_token()}}"},
                success: function () {
                    $('#projectAddModal').modal('hide');
                    toastr.success(title + ", has been succesfully added");
                    getData();
                },
                error: function (error) {
                    console.log("Error: " + error);
                    toastr.error("Something went wrong while adding the project");
                }
            });
        }

        function deleteProjct(id) {
            $.ajax({
                method: "post",
                url: "/project/delete/" + id,
                data: {_token: "{{csrf_token()}}"},
                success: function (response) {
                    toastr.success(response.title + ", has been removed successfully");
                    getData();
                },
                error: function (error) {
                    console.log("Error: " + error);
                    toastr.error("Something went wrong while deleting your project");
                }
            });
        }

        function changeProject(id, title, description) {
            $.ajax({
                method: "post",
                url: "/project/edit/" + id + "/" + title + "/" + description,
                data: {_token: "{{csrf_token()}}"},
                success: function (response) {
                    $('#projectChangeModal').modal('hide');
                    toastr.success(response.title + ", has been successfully updated");
                    getData();
                },
                error: function (error) {
                    console.log("Error: " + error);
                    toastr.error("Something went wrong while updating your project");
                }
            });
        }

        function formValidateTitle(id) {
            let title = document.querySelector("#"+id);

            if (title.value.length <= 0) {
                title.classList.remove('is-valid');
                title.classList.add('is-invalid');
            } else {
                title.classList.remove('is-invalid');
                title.classList.add('is-valid');
            }
        }

        function formValidateDescription(id) {
            let description = document.querySelector("#"+id);

            if (description.value.length <= 0) {
                description.classList.remove('is-valid');
                description.classList.add('is-invalid');
            } else {
                description.classList.remove('is-invalid');
                description.classList.add('is-valid');
            }
        }

        function formValidate(titleId, descriptionId, action) {
            let title = document.querySelector("#"+titleId);
            let description = document.querySelector("#"+descriptionId);

            if (title.value.length > 0 && description.value.length > 0) {
                if(action === "add") {
                    addProject(title.value, description.value);
                    title.value = '';
                    description.value = '';
                    description.classList.remove('is-valid');
                    title.classList.remove('is-valid');
                } else {
                    let id = document.querySelector('#projectChangeId').value;
                    let title = document.querySelector('#inputTitleProjectChange').value;
                    let description = document.querySelector('#inputdescriptionProjectChange').value;
                    changeProject(id, title, description);
                }
            } else {
                if (title.value.length <= 0) {
                    title.classList.add('is-invalid');
                }
                if (description.value.length <= 0) {
                    description.classList.add('is-invalid');
                }
            }
        }

    </script>
@endsection

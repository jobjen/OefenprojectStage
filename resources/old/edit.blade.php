@extends('views.layout')
@section('title', 'Edit the project')

@section('content')
    <div class="container">
        <h1 class="padding-top-normal">{{$project->title}}</h1>

        <form action="/projects/{{$project->id}}" method="post" id="form" class="margin-top-high">
            @csrf
            @method('patch')
            <div class="form-group">
                <label for="inputTitle">The title of the project</label>
                <input type="text" class="form-control" name="title" id="inputTitle" onkeyup="formValidateTitle()" value="{{$project->title}}" required>
                <div class="invalid-feedback"><i class="fas fa-times"></i>&nbsp; Fill in a valid title</div>
                <div class="valid-feedback"><i class="fas fa-check"></i></div>
            </div>

            <div class="form-group">
                <label for="inputdescription">The description of the project</label>
                <textarea type="text" class="form-control" name="description" onkeyup="formValidateDescription()" id="inputdescription" rows="5" required>{{$project->description}}</textarea>
                <div class="invalid-feedback"><i class="fas fa-times"></i>&nbsp; Fill in a valid description</div>
                <div class="valid-feedback"><i class="fas fa-check"></i></div>
            </div>

            <div class="form-group">
                <button type="button" onclick="formValidate()" class="button"><span>Update project</span></button>
            </div>
        </form>
    </div>
@endsection

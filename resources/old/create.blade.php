@extends('views.layout')
@section('title', 'New project')

@section('content')
    <div class="container">
        <h1 class="padding-top-normal">Create a new project</h1>

        <form action="/projects" method="post" id="form" class="margin-top-high">
            @csrf
            <div class="form-group">
                <label for="inputTitle">Project title</label>
                <input type="text" name="title" onkeyup="formValidateTitle()" id="inputTitle" class="form-control {{$errors->any() ? $errors->has('title') ? 'is-invalid' : 'is-valid' : ''}}" value="{{old('title')}}" placeholder="Project title">
                <div class="invalid-feedback"><i class="fas fa-times"></i>&nbsp; Fill in a valid title</div>
                <div class="valid-feedback"><i class="fas fa-check"></i></div>
            </div>

            <div class="form-group">
                <label for="inputdescription">Project description</label>
                <textarea name="description" onkeyup="formValidateDescription()" id="inputdescription" class="form-control {{$errors->any() ? $errors->has('description') ? 'is-invalid' : 'is-valid' : ''}}" placeholder="Project description">{{old('description')}}</textarea>
                <div class="invalid-feedback"><i class="fas fa-times"></i>&nbsp; Fill in a valid description</div>
                <div class="valid-feedback"><i class="fas fa-check"></i></div>
            </div>

            <div class="form-group">
                <button type="button" onclick="formValidate()" class="button"><span>Create project</span></button>
            </div>
        </form>


        @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
@endsection

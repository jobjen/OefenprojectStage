<?php

namespace App\Http\Controllers;

use App\Project;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use stdClass;

class ProjectsController extends Controller
{
    public function __contruct()
    {
        $this->middelware('auth');
    }

    public function index()
    {
        $projects = Project::orderByDesc('id')->get();

        return view('index', compact('projects'));
//        return view('index', [
//        'projects' => auth()->user()->projects
//        ]);
    }

    public function show(Project $project)
    {
        return view('view', compact('project'));
    }

    public function store()
    {
        request()->validate([
            'title' => 'required',
            'description' => 'required'
        ]);

        Project::create(request(['title', 'description']));

        return redirect('projects/');
    }

    public function update(Project $project)
    {
        $project->title = request('title');
        $project->description = request('description');
        $project->save();

        return redirect('/');
    }

    public function destroy($id)
    {
        Project::findOrFail($id)->delete();
        return redirect('/');
    }

    public function getProjectData()
    {
        $projects = Project::where('owner_id', auth()->id())->orderBy('id', 'desc')->get();

        $return_data = new stdClass();
        $return_data->project = $projects;

        $json_encode = json_encode($return_data, JSON_PRETTY_PRINT);

        return response()->json([$json_encode]);
    }

    public function delete($id)
    {
        $project = Project::findOrFail($id);
        $project->delete();

        Task::where('project_id', $id)->delete();

        return $project;
    }

    public function addProject($title, $description)
    {
        $user = auth()->user();

        $project = new Project;
        $project->owner_id = $user->id;
        $project->title = $title;
        $project->description = $description;
        $project->save();
    }

    public function editProject($id, $title, $description)
    {
        $project = Project::findOrFail($id);
        $project->title = $title;
        $project->description = $description;
        $project->save();
    }

    public function getProjectDataById($id)
    {
        $projects = DB::table('projects')->where('id', $id)->get();

//        $projects = Project::all()->where('id', $id);

        $return_data = new stdClass();
        $return_data->project = $projects;

        $json_encode = json_encode($return_data, JSON_PRETTY_PRINT);

        return response()->json([$json_encode]);
    }

    public function checkUserPermissions($project_id)
    {
        $user = auth()->user();
        $user_id = $user->id;

        $project = Project::where('id', $project_id)->get();

        $return_data = new stdClass();
        $return_data->project = $project;
        $json_encode = json_encode($return_data, JSON_PRETTY_PRINT);

        return response()->json([$json_encode]);
    }
}

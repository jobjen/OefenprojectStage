<?php

namespace App\Http\Controllers;

use App\Task;
use App\Project;
use Illuminate\Support\Facades\DB;
use stdClass;

class ProjectsTasksController extends Controller
{
    public static function check($id)
    {
        $task = Task::findOrFail($id);
        $task->completed = 1;
        $task->save();
    }

    public function getData($id)
    {
        $tasks = DB::table('tasks')->where('project_id', '=', $id)->orderBy('completed', 'asc')->get();

        $return_data = new stdClass();
        $return_data->task = $tasks;

        $json_encode = json_encode($return_data, JSON_PRETTY_PRINT);

        return response()->json([$json_encode]);
    }

    public function add($id, $description)
    {
        $task = new Task;
        $task->project_id = $id;
        $task->description = $description;
        $task->save();
    }

    public function update($id, $checked) {

        if ($checked === "true") {
            $completed = 'checked';
        } else {
            $completed = null;
        }

        $task = Task::findOrFail($id);
        $task->completed = $completed;
        $task->save();

        return $checked;
    }
}

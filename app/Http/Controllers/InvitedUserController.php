<?php

namespace App\Http\Controllers;

use App\InvitedUser;
use Illuminate\Http\Request;

class InvitedUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvitedUser  $invitedUser
     * @return \Illuminate\Http\Response
     */
    public function show(InvitedUser $invitedUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvitedUser  $invitedUser
     * @return \Illuminate\Http\Response
     */
    public function edit(InvitedUser $invitedUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvitedUser  $invitedUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvitedUser $invitedUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvitedUser  $invitedUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvitedUser $invitedUser)
    {
        //
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/create', 'ProjectsController@create');
//Route::get('/{project}', 'ProjectsController@show');
//Route::get('/{project}/edit', 'ProjectsController@edit');
//Route::get('/{project}/edit', 'ProjectsController@test');
//Route::patch('projects/{project}', 'ProjectsController@update');
//Route::delete('projects/{project}', 'ProjectsController@destroy');
//Route::post('/store', 'ProjectsController@store');


Route::get('/', 'ProjectsController@index');
Route::get('/project/{project}', 'ProjectsController@show');

Route::post('/task/{id}/add', 'ProjectsTasksController@add');

Route::get('/get-data/{id}', 'ProjectsTasksController@getData');
Route::get('/getProjectData', 'ProjectsController@getProjectData');


Route::post('/project/delete/{id}', 'ProjectsController@delete');
Route::post('/project/add/{title}/{description}', 'ProjectsController@addProject');
Route::get('/getProjectDataById/{id}', 'ProjectsController@getProjectDataById');

Route::post('/project/edit/{id}/{title}/{description}', 'ProjectsController@editProject');

Route::post('/add/{id}/{description}', 'ProjectsTasksController@add');

Route::post('/update/{id}/{checked}', 'ProjectsTasksController@update');

Auth::routes();

Route::get('/check/{id}', 'ProjectsController@checkUserPermissions');
Route::get('/home', 'HomeController@index')->name('home');
